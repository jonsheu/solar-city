Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'user_responses#index'
  get 'index' => 'user_responses#index'
  get 'index.html' => 'user_responses#index'
  post 'user_responses' => 'user_responses#create', as: :user_responses
end
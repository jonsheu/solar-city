class CreateResponse < ActiveRecord::Migration[5.0]
  def change
    create_table :user_responses do |t|
      t.string :name
      t.string :phone_number
      t.string :email
      t.integer :age
      t.string :address
      t.string :city
      t.string :state
      t.integer :zip
    end
  end
end

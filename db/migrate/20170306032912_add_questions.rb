class AddQuestions < ActiveRecord::Migration[5.0]
  def change
    add_column :user_responses, :question_1, :string
    add_column :user_responses, :question_2, :string
  end
end

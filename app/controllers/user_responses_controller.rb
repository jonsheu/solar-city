class UserResponsesController < ApplicationController
  protect_from_forgery with: :exception

  def index
    @response = UserResponse.new
  end

  def create
    @response = UserResponse.create(response_params)
    flash[:notice] = "Your response has been recorded!"
    redirect_to :index
  end

  private

  def response_params
    params.require(:user_response).permit(:name, :email, :phone_number, :age, :address, :city, :state, :zip, :question_1, :question_2)
  end
end
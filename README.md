Solar Sales Coding Challenge
---------------------------

### Problem Statement

* Problem is that interested customers need a way to provide their contact info and description of their interest in home solar panels.
* Solution is providing a simple form to collect their info and record their responses.

### Technical Notes

* Deployed to Heroku at https://solar-sales.herokuapp.com . Since it's running on the free tier, it may take up to 30 seconds for the dyno to wake from sleep.
* I decided to focus my submission on "full stack development" with a clean form interface and Ruby on Rails backend.
* Backend tools used: Ruby on Rails framework, PostgreSQL database
* Frontend tools used: HAML, Bootstrap, SASS/CSS
* I choose to use this stack because I am very familiar with it and have used it for many projects. I made it a standard Ruby on Rails app because I think Rails is great for small projects and easy to expand upon rapidly. I also like that Rails definitely encourages an MVC architecture, which makes development easier. I used a PostgreSQL database to run on Heroku. I used HAML and Bootstrap because it would provide a functional, responsive form for these basic inputs.
* I used the standard Rails app starter generator, so the code that I wrote specifically for this project is mostly in:
  + app/controllers/user_responses_controller.rb
  + app/models/user_response.rb
  + app/views/layouts/application.html.haml
  + app/views/user_responses/index.html.haml  
  + app/assets/stylesheets/application.css.sass
  + app/config/routes.rb
* Trade-offs and further work: Some of the trade-offs are potential scalability issues with Rails and needing to stick with this set of tools instead of a different stack. This may feel like too much overhead for a simple form, but it is easy to expand upon for further applications such as viewing the customer responses. If I were to expand on this project, I would probably add additional form validation for the user input and maybe even interface elements like a map view for the customer's address. To make the app functional, it would also need some way to pass along the contact info in the database to someone such as a sales representative.

* I have some additional code samples on my GitHub page: https://github.com/jshoe?tab=repositories

### How to Run Locally

* As a standard Rails app, you can start it locally with "bundle install" and "rails server".
